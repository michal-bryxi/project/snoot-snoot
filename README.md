# Snoot Snoot

Simple "tap the pairs" game to showcase following technologies:

 - [EmberJS 3.22](https://emberjs.com/)
 - [Font Awesome 5](https://github.com/FortAwesome/ember-fontawesome)
 - [Frontile 0.6.0](https://frontile.dev/versions/master/)
 - [Xstate 4.12.0](https://ember-statecharts.com/)
 - [tailwindcss 1.9.6](hhttps://tailwindcss.com/)
 - [Animate.css 4](https://github.com/sinankeskin/ember-animate-css)

 You can play it online on: https://snoot-snoot.netlify.app

![Game demo animation](./doc/snoot-snoot.gif)

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/) (with npm)
* [Ember CLI](https://ember-cli.com/)
* [Google Chrome](https://google.com/chrome/)

## Installation

* `git clone <repository-url>` this repository
* `cd snootsnoot`
* `npm install`

## Running / Development

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).
* Visit your tests at [http://localhost:4200/tests](http://localhost:4200/tests).

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Linting

* `npm run lint:hbs`
* `npm run lint:js`
* `npm run lint:js -- --fix`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

Specify what it takes to deploy your app.

## Further Reading / Useful Links

* [ember.js](https://emberjs.com/)
* [ember-cli](https://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
