import Component from '@glimmer/component';
import faker from 'faker';

const COLORS = [
  'blue',
  'indigo',
  'gray',
  'purple',
  'red',
  'yellow',
  'green',
  'blue',
  'indigo',
  'purple'
];

export default class PillComponent extends Component {
  get color() {
    return faker.random.arrayElement(COLORS);
  }
}
