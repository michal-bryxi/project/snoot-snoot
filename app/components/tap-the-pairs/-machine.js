import { Machine } from 'xstate';

const machine = Machine(
  {
    id: 'statechart',
    initial: 'notStarted',
    states: {
      notStarted: {
        entry: ['initialize'],
        on: {
          START: 'idle'
        }
      },
      idle: {
        on: {
          SELECT: [
            { target: 'addingNew', cond: 'isSame' },
            { target: 'noping'  }
          ]
        },
      },
      noping: {
        entry: ['noped'],
        on: {
          NEXT: [
            { target: 'idle', cond: 'isAlive' },
            { target: 'lost' }
          ]
        }
      },
      addingNew: {
        entry: ['addNew'],
        on: {
          SCORE: [
            { target: 'score', cond: 'isScore' },
            { target: 'idle' }
          ]
        }
      },
      score: {
        entry: ['scored'],
        on: {
          NEXT: [
            { target: 'won', cond: 'isWin' },
            { target: 'idle' }
          ]
        }
      },
      won: {
        on: {
          REBOOT: [
            { target: 'notStarted' }
          ]
        }
      },
      lost: {
        on: {
          REBOOT: [
            { target: 'notStarted' }
          ]
        }
      }
    }
  }, {
    guards: {
      isSame() {},
      isScore() {},
      isSuccess() {},
      isNope() {},
      isWin() {},
      isAlive() {}
    },
    actions: {
      addNew() {},
      scored() {},
      noped() {},
      initialize() {}
    },
  }
);

export default machine;
