import Component from '@glimmer/component';

import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked, cached } from '@glimmer/tracking';
import tapThePairsMachine from './-machine';
import { use } from 'ember-usable';
import { matchesState, useMachine } from 'ember-statecharts';

export default class GameComponent extends Component {
  @service store;
  @service heap;

  @tracked livesCounter = 3;
  @tracked scoreCounter = 0;

  maxLives = 3;
  width = 5;
  height = 6;
  maxSameTiles = 4;

  @cached
  get tiles() {
    return this.heap.tiles;
  }

  @matchesState('notStarted') isNotStarted;
  @matchesState('won') isWon;
  @matchesState('lost') isLost;

  @use statechart = useMachine(tapThePairsMachine)
    .withContext({
      component: this
    })
    .withConfig({
      actions: {
        scored: this.scored,
        addNew: this.addNew,
        noped: this.noped,
        initialize: this.initialize
      },
      guards: {
        isSame: this.isSame,
        isScore: this.isScore,
        isSuccess: this.isSuccess,
        isNope: this.isNope,
        isWin: this.isWin,
        isAlive: this.isAlive
      }
    });

  get goal() {
    return this.uniqueHashes.length;
  }

  get uniqueHashes() {
    return Array.from(
      new Set(
        this.tiles.map((tile) => tile.hash)
      )
    );
  }

  get selectedTiles() {
    return this.tiles.filterBy('isSelected', true);
  }

  get selectedHash() {
    return this.selectedTiles?.firstObject?.hash
  }

  get selectedTilesWithSelectedHash() {
    return this.tiles.filterBy('hash', this.selectedHash);
  }

  isSame({component}, {newTile}) {
    return component.selectedTiles.length === 0
      || component.selectedTiles.every((t) => t.hash === newTile.hash);
  }

  isScore({component}) {
    return component.selectedTiles.length >= component.selectedTilesWithSelectedHash.length;
  }

  isWin({component}) {
    return component.scoreCounter >= component.goal;
  }

  isAlive({component}) {
    return component.livesCounter > 0;
  }

  scored({component}) {
    for(const tile of component.selectedTiles) {
      tile.isSelected = false;
      tile.isEliminated = true;
    }
    component.scoreCounter = component.scoreCounter + 1;

    component.statechart.send('NEXT')
  }

  noped({component}) {
    for(const tile of component.selectedTiles) {
      tile.isSelected = false;
    }
    component.livesCounter = component.livesCounter - 1;
    component.statechart.send('NEXT');
  }

  initialize({component}) {
    let {width, height, maxSameTiles} = component;
    component.heap.generate(width, height, maxSameTiles)
  }

  addNew({component}, {newTile}) {
    newTile.isSelected = true;
    component.statechart.send('SCORE')
  }

  @action startGame() {
    this.statechart.send('START');
  }

  @action reboot() {
    this.statechart.send('REBOOT');
  }

  @action tileSelected(newTile) {
    this.statechart.send('SELECT', {newTile});
  }
}
