import Component from '@glimmer/component';

export default class TapThePairsLivesComponent extends Component {
  get lives() {
    return [ ...Array(this.args.maxLives).keys() ].map((i) => {
      if(i >= this.args.livesCounter) {
        return {
          icon: 'heart-broken',
          color: 'red'
        }
      } else {
        return {
          icon: 'heart',
          color: 'green'
        }
      }
    });
  }
}
