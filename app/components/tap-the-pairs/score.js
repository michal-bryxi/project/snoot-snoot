import Component from '@glimmer/component';

export default class TapThePairsScoreComponent extends Component {
  get points() {
    return [ ...Array(this.args.goal).keys() ].map((i) => {
      if(i >= this.args.score) {
        return {
          icon: 'cloud',
          color: 'gray'
        }
      } else {
        return {
          icon: 'sun',
          color: 'yellow'
        }
      }
    });
  }
}
