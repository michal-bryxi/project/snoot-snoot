import Model from '@ember-data/model';
import { attr } from '@ember-data/model';

export default class TileModel extends Model {
  @attr('string') icon;
  @attr('string') color;
  @attr('string') bgColor;
  @attr('string') animation;
  @attr('string') size;
  @attr('number') rotation;
  @attr('boolean') isSelected;
  @attr('boolean') isEliminated;
  @attr('string') uuid;

  get hash() {
    return `${this.icon}-${this.color}-${this.bgColor}-${this.rotation}-${this.size}`;
  }
}
