import Service from '@ember/service';
import faker from 'faker';
import { inject as service } from '@ember/service';

const ICONS = [
  'crow', 'kiwi-bird', 'dove', 'cat', 'fish', 'horse', 'spider', 'frog', 'horse-head', 'dog', 'hippo', 'otter'
];

const COLORS = [
  'yellow',
  'green',
];

const SIZES = [
  '1x',
  '2x'
]

const ROTATIONS = [
  0,
  90,
  180,
  270
];

const ANIMATIONS = [
  'flash', 'pulse', 'backInDown', 'backInLeft', 'backInRight', 'backInUp', 'bounceIn', 'flip', 'lightSpeedInRight', 'rotateIn', 'zoomIn', 'slideInDown'
];

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
  }
}

export default class HeapService extends Service {
  @service store;

  get tiles() {
    const allTiles = [...this.store.peekAll('tile').toArray()];
    shuffleArray(allTiles);
    return allTiles;
  }

  generate(width, height, maxSameTiles) {
    const uniqueTilesCount = width * height / 2;
    const tilesCount = uniqueTilesCount * 2;
    let generated = 0;
    let spec;
    let count = 0;

    while(generated < tilesCount) {
      if(count <= 0) {
        count = this.randomCount(maxSameTiles);
        spec = this.randomSpec;
      }

      this.store.createRecord('tile', spec);

      count = count - 1;
      generated = generated + 1;
    }
  }

  randomCount(maxSameTiles) {
    return faker.random.number({min: 2, max: maxSameTiles})
  }

  get randomSpec() {
    return {
      icon: this.randomIcon,
      color: this.randomColor,
      bgColor: this.randomColor,
      rotation: this.randomRotation,
      size: this.randomSize,
      animation: this.randomAnimation,
      isSelected: false,
      isEliminated: false,
      uuid: this.randomId
    };
  }

  get randomId() {
    return faker.random.uuid();
  }

  get randomIcon() {
    return faker.random.arrayElement(ICONS);
  }

  get randomSize() {
    return faker.random.arrayElement(SIZES);
  }

  get randomRotation() {
    return faker.random.arrayElement(ROTATIONS);
  }

  get randomColor() {
    return faker.random.arrayElement(COLORS);
  }

  get randomAnimation() {
    return faker.random.arrayElement(ANIMATIONS);
  }
}
