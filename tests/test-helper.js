import Application from 'snootsnoot/app';
import config from 'snootsnoot/config/environment';
import { setApplication } from '@ember/test-helpers';
import { start } from 'ember-qunit';

setApplication(Application.create(config.APP));

start();
